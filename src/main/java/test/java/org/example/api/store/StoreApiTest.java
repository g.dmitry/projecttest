package org.example.api.store;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.example.model.Pet;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

import static io.restassured.RestAssured.given;

public class StoreApiTest {
    private Pet pet = new Pet();
    private int id = 5;
    private int petId = 6;
    private int quantity = 7;
    private String complete = "true";


    @BeforeClass
    public void setUp() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("my.properties"));
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://petstore.swagger.io/v2/")
                .addHeader("api_key", System.getProperty("api.key"))

                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.filters(new ResponseLoggingFilter());
    }

    @Test
    public void StoreApiTest() {
        pet.setId(id);
        pet.setPetId(petId);
        pet.setQuantity(quantity);
        pet.setComplete(complete);
        given()
                .body(pet)
                .when()
                .post("/store/order")
                .then()
                .statusCode(HttpStatus.SC_OK);

            Pet actiual = given()
                    .pathParam("orderId", id)
                    .when()
                    .get("/store/order/{orderId}")
                    .then()
                    .statusCode(HttpStatus.SC_OK)
                    .extract()
                    .body()
                    .as(Pet.class );
        Assert.assertEquals(actiual.getId(), pet.getId());

    }

    @Test
    public void deleteOrderTest() {

            given()
                    .pathParam("orderId", id)
                    .when()
                    .delete("/store/order/{orderId}")
                    .then()
                    .statusCode(200);

            given()
                    .pathParam("orderId", id)
                    .when()
                    .get("/store/order/{orderId}")
                    .then()
                    .statusCode(404);

    }

}