package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {
    public Herbivore(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    public void eat(Food food) {
        if (food instanceof Meat) {
            throw new WrongFoodException("Травоядные не едят траву");
        } else {
            satiety += food.getFoodEnergy();
            System.out.println("Травоядное поел его сытость: " + satiety);
        }
    }
}