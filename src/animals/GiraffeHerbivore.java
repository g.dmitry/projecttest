package animals;

public class GiraffeHerbivore extends Herbivore implements Run{



    public GiraffeHerbivore(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void run() {
        System.out.println("Жираф бежит");
    }
}
