package animals;

import food.Food;
import food.WrongFoodException;

import java.util.Objects;

public abstract class Animal {


    private final int sizeAnimal;
    protected String name;
    protected int satiety;

    protected Animal(String name, int sizeAnimal) {
        this.name = name;
        this.sizeAnimal = sizeAnimal;
    }

    public abstract void eat(Food food);

    public String getName() {
        return name;
    }

    public int getSatiety() {
        return satiety;
    }

    public int getSizeAnimal() {
        return sizeAnimal;
    }




}
