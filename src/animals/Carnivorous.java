package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {


    protected Carnivorous(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            throw new WrongFoodException("Хищники не едят траву");
        } else {
            satiety += food.getFoodEnergy();
            System.out.println("Хищник поел его сытость: " + satiety);
        }
    }

}
