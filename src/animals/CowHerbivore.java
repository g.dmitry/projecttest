package animals;

public class CowHerbivore extends Herbivore implements Run, Voice{


    public CowHerbivore(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void run() {
        System.out.println("Корова бежит");
    }

    @Override
    public String voice() {
        return "Муууууу";
    }


}
