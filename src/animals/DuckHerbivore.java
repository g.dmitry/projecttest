package animals;

public class DuckHerbivore extends Herbivore implements Fly, Swim, Run, Voice{



    public DuckHerbivore(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void fly() {
        System.out.println("Утка летает");
    }

    @Override
    public void run() {
        System.out.println("Утка бежит");
    }

    @Override
    public void swim() {
        System.out.println("Утка плывет");
    }

    @Override
    public String voice() {
        return "Кряяя";
    }
}
