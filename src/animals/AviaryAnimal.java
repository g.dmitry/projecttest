package animals;

import java.util.HashMap;
import java.util.logging.Logger;

public class AviaryAnimal<T extends Animal> {

    private int size;

    private HashMap<String, Animal> map = new HashMap<>();

    public AviaryAnimal(SizeAviary sizeAviary) {
        size = sizeAviary.getSize();
    }

    public void putAnimal(Animal animal) {
        if (size >= animal.getSizeAnimal()) {
            map.put(animal.getName(), animal);
            size -= animal.getSizeAnimal();
            System.out.println("В вольер добавлен " + animal.name);
        } else {
            System.out.println("Вольер слишком маленький");
        }
    }

    public void removeAnimal(String name) {
        map.remove(name);
        System.out.println(name + " удалился из вольера");
    }

    public Animal getAnimal(String name) {
        System.out.println("Вызван " + name);
        return map.get(name);
    }
}
