package animals;

public class WolfCarnivorous extends Carnivorous implements Run,Swim,Voice {



    public WolfCarnivorous(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void run() {
        System.out.println("Волк бежит");
    }

    @Override
    public void swim() {
        System.out.println("Волк плавает");
    }

    @Override
    public String voice() {
        return "Уууу";
    }
}
