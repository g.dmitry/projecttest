package animals;

public enum SizeAviary {
    SMALL(4),
    BIG(6),
    VERY_SMALL(2),
    VERY_BIG(8);

    public int getSize() {
        return size;
    }

    SizeAviary(int size) {
        this.size = size;
    }

    private int size;


}
