package animals;

public class TigerCarnivorous extends Carnivorous implements Run, Swim,Voice {



    public TigerCarnivorous(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void run() {
        System.out.println("Тигр бежит");
    }

    @Override
    public void swim() {
        System.out.println("Тигр плывет");
    }

    @Override
    public String voice() {
        return "'Рычит'";
    }
}
