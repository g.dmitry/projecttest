package animals;

public class PiranhaCarnivorous extends Carnivorous implements Swim{



    public PiranhaCarnivorous(String name, int sizeAnimal) {
        super(name, sizeAnimal);
    }

    @Override
    public void swim() {
        System.out.println("Пиранья плавает");
    }
}
