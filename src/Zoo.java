import animals.*;
import food.*;

public class Zoo {
    public static void main(String[] args) {
        CowHerbivore cow = new CowHerbivore("Буренка", 4);
        DuckHerbivore duck =  new DuckHerbivore("Скрудж", 2);
        GiraffeHerbivore giraffe = new GiraffeHerbivore("Мелман", 4);
        PiranhaCarnivorous piranha = new PiranhaCarnivorous("Пиранья", 1);
        TigerCarnivorous tiger = new TigerCarnivorous("Шерхан", 5);
        WolfCarnivorous wolf = new WolfCarnivorous("Ракша",3);
        Worker worker = new Worker();
        Apple apple = new Apple();
        Beef beef = new Beef();
        Chicken chicken = new Chicken();
        Plantain plantain = new Plantain();
        Pork pork = new Pork();
        Sedge sedge = new Sedge();
        AviaryAnimal<Animal> aviaryAnimal = new AviaryAnimal<>(SizeAviary.VERY_BIG);
        aviaryAnimal.putAnimal(tiger);
        aviaryAnimal.putAnimal(duck);
        aviaryAnimal.getAnimal("Шерхан");
        aviaryAnimal.removeAnimal("Буренка");


        Animal[] animals = {cow, duck, giraffe, piranha, tiger, wolf};
        Food[] foods = {apple, beef, chicken, plantain, pork, sedge};
        for (int i = 0; i < animals.length; i++) {
            for (int j = 0; j < foods.length; j++) {
                worker.feed(animals[i],foods[j]);
            }
        }
        Swim[] pool = {duck, piranha, tiger, wolf};
        for (int i = 0; i < pool.length; i++) {
            pool[i].swim();
        }
        System.out.println("Корова " + cow.getSatiety());



    }

}
